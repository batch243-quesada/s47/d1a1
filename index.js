// Document Object Model
// It allows us to access or modify the properties of an html element in a webpage
// We use this because it is standard on how to get, change, add, or delete html elements.
// We will focus on using DOM in managing forms.

// Syntax:
// document.querySelector("html element")
// For selecting HTML elements, we will be using document.querySelector
// The quesrySelector function takes a string input that is formatted like a css selector when applying the styles

// const txtFirstName = document.querySelector("#txt-first-name");
// console.log(txtFirstName);

// const txtLastName = document.querySelector("#txt-last-name");

// const name = document.querySelectorAll(".full-name");
// // console.log(name);

// const span = document.querySelectorAll("span");
// // console.log(span);

// const text = document.querySelectorAll("input[type]");
// console.log(text);

// const spanFullName = document.querySelector("#fullName");

// EVENT LISTENERS
// - Whenever a user interacts with a webpage, this action is considered as an event.
// - Working with events is large part of creating interactivity in a webpage.
// - Specific functions that will perform an action

// - The function use is "addEventListener" - it has two arguments
// 	1. First argument -  a string identifying an event. Will declare an event.
// 	2. Second argument - function that the listener will trigger once the "specific event" is triggered. 

	const txtFirstName = document.getElementById("txt-first-name");
	const txtLastName = document.getElementById("txt-last-name");
	const spanFullName = document.getElementById("fullName");

	const fullName = () => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	}

	txtFirstName.addEventListener("keyup", fullName);
	txtLastName.addEventListener("keyup", fullName);

// ------------------------------------------------------------- //

	// const changeColor = () => {
	// 	if (black.onclick) spanFullName.classList.add('black');
	// 	else if (red.onclick) spanFullName.classList.add('red');
	// 	else if (blue.onclick) spanFullName.classList.add('blue');
	// 	else if (green.onclick) spanFullName.classList.add('green');
	// }

	const colour = document.getElementById("colour")

	colour.addEventListener("click", () => {
		spanFullName.style.color = colour.value;
	});